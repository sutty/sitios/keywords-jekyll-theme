---
title: About This Site
layout: page
uuid: 43f8aa97-225c-4e9a-bc73-1ab16d56785c
last_modified_at: 2016-02-01 17:46:01.000000000 +00:00
book: 4b5534c6-21a6-4598-b134-d06081de34e6
migration_id: 4631
permalink: "/asian-american-studies/about-this-site/"
---

Born out of the Civil Rights and Third World Liberation movements of the 1960s and 1970s, Asian American Studies has grown significantly over the past four decades, both as a distinct field of inquiry and as a potent site of critique. Characterized by transnational, trans-Pacific, and trans-hemispheric considerations of race, ethnicity, migration, immigration, gender, sexuality, and class, this multidisciplinary field engages with a set of concepts profoundly shaped by past and present histories of racialization and social formation.

The keywords included in this collection are central to social sciences, humanities, and cultural studies and reflect the ways in which Asian American Studies has transformed scholarly discourses, research agendas, and pedagogical frameworks. Spanning multiple histories, numerous migrations, and diverse populations, _Keywords for Asian American Studies_ reconsiders and recalibrates the ever-shifting borders of Asian American studies as a distinctly interdisciplinary field.

The print publication includes 61 essays, each on a term integral to the field such as “diaspora,” “assimilation,” and “orientalism.” The site includes the volume’s [“Introduction,”](/asian-american-studies/introduction/ "Introduction") ten web essays from the volume, the list of [works cited](/asian-american-studies/works_cited/) for all the essays, information about the [contributors](/asian-american-studies/contributors/ "Contributors"), a [note on classroom use](/asian-american-studies/in-the-classroom/note-on-classroom-use/ "Note on Classroom Use"). Any page in the site can be printed or saved as a pdf, and a single click provides a citation to that page that can be pasted into a bibliography.

### **Explore the Site**  

Readers may browse the full list of essays by clicking **Essays** at the upper left **to bring up a menu**. Clicking **Search** at upper right allows you to discover both the print and web essays: search results show the web essays in full and snippets from the print essays, with a page reference. This function enables readers to discover connections among the complete set of essays in this book.

In addition, the site enables readers to discover connections and contrasts across **all** the different Keywords volumes. Readers may select which books they want to explore. For example, if you select "all books" and search for "education," you'll see how that term is used not only in Asian American studies (**[education](/asian-american-studies/essay/education/)**), but also in children's literature (**[education](/childrens-literature/essay/education/)**), disability studies (**[education](/disability-studies/essay/education/)**), and environmental studies (**[education](/environmental-studies/essay/education/)**).

### **Courses**

Instructors may wish to consult the [“Note on Classroom Use”](/asian-american-studies/in-the-classroom/note-on-classroom-use/ "Note on Classroom Use") to find suggestions for how to employ this book in the classroom.

### **The Book**

Readers can learn more about the print book, and purchase a copy of it from NYU Press, by clicking on the shopping cart logo at the top of each page.

### **Communication**

You can share links using Facebook, Twitter, Google Plus, and e-mail from any page in this site.

