---
title: About this Site
layout: page
uuid: 303affda-ce45-4bb6-801e-a50b59ac21da
last_modified_at: 2017-03-22 16:20:59.000000000 +00:00
book: 71d5b4a3-c52d-4f4e-b9c6-cecee97badec
migration_id: 2208
authors:
- 213f213b-5f54-49d2-b864-e5002939036a
- c85db878-13f0-42c5-882f-bfd998700cf5
permalink: "/media-studies/about-this-site/"
---

_Keywords for Media Studies_ introduces and aims to advance the field of critical media studies by tracing, defining, and problematizing its established and emergent terminology. The book historicizes thinking about media and society, whether that means noting a long history of “new media,” or tracing how understandings of media “power” vary across time periods and knowledge formations.

The print publication includes sixty-five essays from an impressive group of established scholars from television studies, film studies, sound studies, games studies, and more. Each of the essays in the volume focuses on a critical concept, from “[**fan**](/media-studies/essay/fan/)” to “[**industry**](/media-studies/essay/industry/),” and “[**celebrity**](/media-studies/essay/celebrity/)” to “[**surveillance**](/media-studies/essay/surveillance/).”

This site includes the volume’s **“[Introduction](/media-studies/introduction/),”** 7 web essays from the volume, the list of **[works cited](/media-studies/works_cited/)** for all the essays, information about the [**contributors**](/media-studies/contributors/), a **[note on classroom](/media-studies/note-on-classroom-use/)** use. Any page in the site can be printed or saved as a PDF, and a single click provides a citation to that page that can be pasted into a bibliography.

### **EXPLORE THE SITE**

Readers may browse the full list of essays by clicking **Essays** at the upper left **to bring up a menu**. Clicking **Search** at upper right allows you to discover both the print and web essays: search results show the web essays in full and snippets from the print essays, with a page reference. This function enables readers to discover connections among the complete set of essays in this book.

 

In addition, the site enables readers to discover connections and contrasts across **all** the different Keywords volumes. Readers may select which books they want to explore. For example, if you select “all books” and search for “identity,” you’ll see how that term is used not only in media studies ([**identity**](/media-studies/essay/identity/)) but also in Asian American studies ([**identity**](/asian-american-studies/essay/identity/)), American Cultural studies ([**identity**](/american-cultural-studies/essay/identity/)), and disability studies ([**identity**](/disability-studies/essay/identity/)).

### **COURSES**

Instructors may wish to consult the **“[Note on Classroom Use](/media-studies/note-on-classroom-use/)”** to find suggestions for how to employ this book in the classroom.

### **THE BOOK**

Readers can learn more about the print book, and purchase a copy of it from NYU Press, by clicking on the shopping cart logo at the top of each page.

### **COMMUNICATION**

You can share links using Facebook, Twitter, Google Plus, and e-mail from any page in this site.

