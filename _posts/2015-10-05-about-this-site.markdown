---
title: About this Site
layout: page
uuid: 83235b6d-7f1b-4728-b24e-8ffd2def5be5
last_modified_at: 2016-02-01 17:46:46.000000000 +00:00
book: c727056c-c66c-42c0-8302-3d699da7b80d
migration_id: 2644
permalink: "/childrens-literature/about-this-site/"
---

The study of children’s literature and culture has been experiencing a renaissance, with vital new work proliferating across many areas of interest. Mapping this vibrant scholarship, _Keywords for Children’s Literature_ presents essays on the essential terms and concepts of the field. From Aesthetics to Young Adult, an impressive, multidisciplinary cast of scholars explores the vocabulary central to the study of children's literature.

Each author traces their keyword’s branching uses and meanings, often into unfamiliar disciplinary territories: Award-winning novelist Philip Pullman writes about Intentionality, Education expert Margaret Meek Spencer addresses Reading, literary scholar Peter Hunt historicizes Children’s Literature, Psychologist Hugh Crago examines Story, librarian and founder of the influential Child\_Lit litserv Michael Joseph investigates Liminality. The scope, clarity, and interdisciplinary play between concepts make this collection essential reading for all scholars in the field.

The print publication includes 49 essays, each on a term integral to the field. The site includes the volume’s **[Introduction](/childrens-literature/introduction/),** five web essays from the volume, the list of **[works cited](/childrens-literature/works_cited/)** for all the essays, information about the [**contributors**](/childrens-literature/contributors/), a **[note on classroom use](/childrens-literature/in-the-classroom/note-on-classroom-use/)**. Any page in the site can be printed or saved as a pdf, and a single click provides a citation to that page that can be pasted into a bibliography.

### **EXPLORE THE SITE**

Readers may browse the full list of essays by clicking **Essays** at the upper left **to bring up a menu**. Clicking **Search** at upper right allows you to discover both the print and web essays: search results show the web essays in full and snippets from the print essays, with a page reference. This function enables readers to discover connections among the complete set of essays in this book.

In addition, the site enables readers to discover connections and contrasts across **all** the different Keywords volumes. Readers may select which books they want to explore. For example, if you select "all books" and search for "education," you'll see how that term is used not only in children's literature (**[education](/childrens-literature/essay/education/)**), but also in Asian American studies (**[education](/asian-american-studies/essay/education/)**), disability studies (**[education](/disability-studies/essay/education/)**), and environmental studies (**[education](/environmental-studies/essay/education/)**).

### **COURSES**

Instructors may wish to consult the **[Note on Classroom Use](/childrens-literature/in-the-classroom/note-on-classroom-use/)** to find suggestions for how to employ this book in the classroom.

### **THE BOOK**

Readers can learn more about the print book, and purchase a copy of it from NYU Press, by clicking on the shopping cart logo at the top of each page.

### **COMMUNICATION**

You can share links using Facebook, Twitter, Google Plus, and e-mail from any page in this site, using the icons at the upper right.
