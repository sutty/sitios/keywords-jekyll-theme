---
title: Acknowledgments
layout: page
uuid: 4fe1f095-bc12-445f-bd69-856303436829
last_modified_at: 2015-04-13 21:16:00.000000000 +00:00
book: 4b5534c6-21a6-4598-b134-d06081de34e6
migration_id: 4649
authors:
- 6266a1ba-bb27-4ea6-940d-d67286973301
- bcff61cf-49bb-4693-9233-3aa4b4ef3b11
- d1f060bc-2aea-4928-83fa-4a441348e3d3
permalink: "/asian-american-studies/acknowledgments/"
---

First and foremost, we want to publicly thank all the contributors to this _Keywords for Asian American Studies_ volume, whose work renders visible the capaciousness, strength, and growth of the field. They patiently worked with us through our requests for revisions to make this a cohesive project and it is through their immense scholarly contributions to the field that we are able to produce this collection.

We likewise owe much to Eric Zinner, who had the foresight to envision the need for such a volume; without hesitation and with considerable consistency, he provided indefatigable support and offered invaluable advice from the planning stage to the production phase. Alicia Nadkarni at NYU Press in comparative fashion ushered us through all facets of the process. This volume benefits greatly from anonymous readers, who productively pushed us to reconsider and reevaluate the overall scope of the project.

In a more local vein, _Keywords for Asian American Studies_ would not be possible without the careful eyes of Laura A. Wright, who vetted citations and kept the project on track in its first phase; we are also appreciative of Patrick S. Lawrence, who made sure the manuscript was thoroughly prepared for final submission. Last, but certainly not least, we want to acknowledge those who make what we do possible via their hourly and daily support:

Cathy is thankful to her parents, Charles and Ginko Schlund, along with her twin brother, Charles; they have offered unfaltering support and guidance. She is forever indebted to Christopher Vials, who is a true partner in all respects.

Linda appreciates her parents, Thuy and Bob, and sister, Christine, and her family for their constant sustenance and encouragement. She is thankful for her children, Aisha and Kian, and partner, John, and his children, Bronson and Carly, who bring her immeasurable enjoyment and fulfillment.

Scott is grateful for the wonderful support he has received over the years from his parents, Henry and Mary Wong, his brothers, Kenny, Keith, and Christopher, and his wife, Carrie, and daughter, Sarah, as well as his friends and colleagues who sustain him with love, companionship, good food, and music.

Finally, it is to our students, mentors, and colleagues that we dedicate this collection for enriching our pedagogical capacities and reminding us of the vitality of Asian American studies.

