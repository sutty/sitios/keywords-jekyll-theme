---
title: Cathy Schlund-Vials
layout: person
uuid: efed3eeb-eab6-4683-998f-9c9fd452b44a
permalink: "/author/cathy-schlund-vials/"
alternate-permalinks:
- "/comics-studies/author/cathy-schlund-vials/"
first_name: Cathy
last_name: Schlund-Vials
books:
- e4cf03db-ef44-443b-baf8-3efa7c3eadcc
posts:
- a0bc5ac3-fcdb-4236-aa20-8f3751e974b1
---

 **Cathy Schlund-Vials** is Board of Trustees Distinguished Professor of English and Asian / Asian American Studies at the University of Connecticut. She is also Associate Dean for Humanities in UConn’s College of Liberal Arts and Sciences. She has co-edited a number of volumes on race, human rights, and historical trauma, including _Redrawing the Historical Past: History, Memory, and Multiethnic Graphic Novels_ and _Keywords for Asian American Studies._