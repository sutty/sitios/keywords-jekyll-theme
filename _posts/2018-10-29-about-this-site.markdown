---
title: About this Site
layout: page
uuid: 9d2ab0b3-b0cd-4212-add4-7662c4a7e43e
last_modified_at: 2019-07-01 15:19:43.000000000 +00:00
book: 6f6daf89-777c-434c-9070-a371f612901c
migration_id: 156
authors:
- 1af8a1f2-d43f-4e28-94b3-029d1a9f3ffd
- bde776c9-d50e-4634-bc5c-e7e65a560595
- 709a01bd-cf98-4188-875f-5ef89556c12d
permalink: "/african-american-studies/about-this-site/"
---

As the longest-standing interdisciplinary field, African American Studies has laid the foundation for critically analyzing issues of race, ethnicity, and culture within the academy and beyond. This volume assembles the keywords of this field for the first time, exploring not only the history of those categories but their continued relevance in the contemporary moment. Taking up a vast array of issues such as [slavery](/african-american-studies/essay/slavery/), [colonialism,](/african-american-studies/essay/colonialism/) [incarceration,](/african-american-studies/essay/incarceration/) [sexuality,](/african-american-studies/essay/sexuality/) [gender,](/african-american-studies/essay/gender/) [feminism,](/african-american-studies/essay/feminism/) [war,](/african-american-studies/essay/war/) and [popular culture](/african-american-studies/essay/popular/), _Keywords for African American Studies_ showcases the startling breadth that characterizes the field.

Featuring an august group of contributors across the social sciences and the humanities, the keywords assembled within the pages of this volume exemplify the depth and range of scholarly inquiry into Black life in the United States. Connecting lineages of Black knowledge production to contemporary considerations of [race](/african-american-studies/essay/race/), [gender,](/african-american-studies/essay/gender/) and [sexuality,](/african-american-studies/essay/sexuality/) _Keywords for African American Studies_ provides a model for how the scholarship of the field can meet the challenges of our social world.

### **EXPLORE THE SITE**

Readers may browse the full list of essays by clicking **Essays** at the upper left **to bring up a menu**. Clicking **Search** at upper right allows you to discover both the print and web essays: search results show the web essays in full and snippets from the print essays, with a page reference. This function enables readers to discover connections among the complete set of essays in this book.

In addition, the site enables readers to discover connections and contrasts across **all** the different Keywords volumes. Readers may select which books they want to explore. For example, if you select “all books” and search for “race,” you’ll see how that term is used not only in African American Studies **([race](/african-american-studies/essay/race/))** but also in Latina/o studies (**[race](/latina-latino-studies/essay/race/)**), Asian American studies (**[race](/asian-american-studies/essay/race/)**), American Cultural studies (**[race](/american-cultural-studies/essay/race/)**), media studies (**[race](/media-studies/essay/race/)**), and disability studies (**[race](/disability-studies/essay/race/)**).

### **COURSES**

Instructors may wish to consult the **“[Note on Classroom Use](/african-american-studies/in-the-classroom/classroom-use/)”** to find suggestions for how to employ this book in the classroom.

### **THE BOOK**

Readers can learn more about the print book, and purchase a copy of it from NYU Press, by clicking on the shopping cart logo at the top of each page.

### **COMMUNICATION**

You can share links using Facebook, Twitter, Google Plus, and e-mail from any page in this site.

