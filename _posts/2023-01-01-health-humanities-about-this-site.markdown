---
title: About this Site
layout: page
uuid: 86baae2e-c1be-4299-ab33-782eaebb4b16
permalink: "/health-humanities/about-this-site/"
book: b2469a22-cbb1-42e4-8343-b57d43badad0
authors:
- 853441c5-ae0d-478c-aafc-8f985eea5807
- 2fa0514c-5d92-4613-9dec-2b6ae476adf8
- 0b6efa3f-7e02-4dcb-b4d2-f62806a880da
---

_Keywords for Health Humanities_ provides a rich, interdisciplinary vocabulary for the burgeoning field of health humanities and, more broadly, for the study of medicine and health. Sixty-five entries by leading international scholars examine current practices, ideas, histories, and debates around health and illness, revealing the social, cultural, and political factors that structure health conditions and shape health outcomes.

Presenting possibilities for health justice and social change, this volume exposes readers—from curious beginners to cultural analysts, from medical students to health care practitioners of all fields—to lively debates about the complexities of health and illness and their ethical and political implications. A study of the vocabulary that comprises and shapes a broad understanding of health and the practices of healthcare, _Keywords for Health Humanities_ guides readers toward ways to communicate accurately and effectively while engaging in creative analytical thinking about health and healthcare in an increasingly complex world—one in which seemingly straightforward beliefs and decisions about individual and communal health represent increasingly contested terrain.
