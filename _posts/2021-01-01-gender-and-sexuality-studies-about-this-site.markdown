---
title: About this Site
layout: page
uuid: 56e422a3-f94d-4613-b9fa-b6ca5724a9d7
permalink: "/gender-and-sexuality-studies/about-this-site/"
book: 95c93490-ffe7-4aa1-a2c6-b300a738df29
authors:
- da31ba8e-4d70-4360-88ff-75e40b17ee4d
---

_Keywords for Gender and Sexuality Studies_ introduces readers to a set of terms that will aid them in understanding the central methodological and political stakes currently energizing feminist and queer studies. The volume deepens the analyses of this field by highlighting justice-oriented intersectional movements and foregrounding Black, Indigenous, and women of color feminisms; transnational feminisms; queer of color critique; trans, disability, and fat studies; feminist science studies; and critiques of the state, law, and prisons that emerge from queer and women of color justice movements.

Many of the keywords featured in this publication call attention to the fundamental assumptions of humanism’s political and intellectual debates—from the racialized contours of property and ownership to eugenicist discourses of improvement and development. Interventions to these frameworks arise out of queer, feminist and anti-racist engagements with matter and ecology as well as efforts to imagine forms of relationality beyond settler colonial and imperialist epistemologies

Reflecting the interdisciplinary breadth of the field, this collection of seventy essays by scholars across the social sciences and the humanities weaves together methodologies from science and technology studies, affect theory, and queer historiographies, as well as Black Studies, Latinx Studies, Asian American, and Indigenous Studies. Taken together, these essays move alongside the distinct histories and myriad solidarities of the fields to construct the much awaited _Keywords for Gender and Sexuality Studies_.
