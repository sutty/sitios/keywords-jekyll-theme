---
title: 'Abelmann, Nancy. 2009. The Intimate University: Korean American Students and
  the Problem of Segregation. Durham, NC: Duke University Press.'
layout: work_cited
uuid: 55a5b9d1-ec25-4a0a-a099-b97e7900d602
last_modified_at: 2015-04-10 18:08:42.000000000 +00:00
book: 4b5534c6-21a6-4598-b134-d06081de34e6
migration_id: 3493
permalink: "/asian-american-studies/works_cited/abelmann-nancy/"
authorship: Abelmann, Nancy.
---

**Abelmann, Nancy.** 2009. _The Intimate University: Korean American Students and the Problem of Segregation_. Durham, NC: Duke University Press.