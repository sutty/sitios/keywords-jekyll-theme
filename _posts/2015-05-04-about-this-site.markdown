---
title: About this Site
layout: page
uuid: a98a682b-1981-4ade-ada7-505bee424607
last_modified_at: 2016-02-01 17:45:23.000000000 +00:00
book: d84a99be-4604-4a4b-964f-9da2951261b1
migration_id: 907
authors:
- 52e53961-abba-436c-9b9a-9fae834fc60d
- d0d5bde7-65ee-4a83-bbd6-c0ac68c0449e
- b1d25079-33f5-4514-8e44-cb98b666e649
permalink: "/disability-studies/about-this-site/"
---

_Keywords for Disability Studies_ aims to broaden and define the conceptual framework of disability studies for readers and practitioners in the field and beyond. The volume engages some of the most pressing debates of our time, such as prenatal testing, euthanasia, accessibility in public transportation and the workplace, post-traumatic stress, and questions about the beginning and end of life.

An invaluable resource for students and scholars alike, _Keywords for Disability Studies_ brings the debates that have often remained internal to disability studies into a wider field of critical discourse, providing opportunities for fresh theoretical considerations of the field’s core presuppositions through a variety of disciplinary perspectives.

The print publication includes 60 essays, each focused on a distinct critical concept, including [“ethics,”](/disability-studies/essay/ethics/ "Ethics") [“medicalization,”](/disability-studies/essay/medicalization/ "Medicalization") [“performance,”](/disability-studies/essay/performance/ "Performance") [“reproduction,”](/disability-studies/essay/reproduction/ "Reproduction") [“identity,”](/disability-studies/essay/identity/ "Identity") and [“stigma,”](/disability-studies/essay/stigma/ "Stigma") among others. This site includes the volume’s [“Introduction,”](/disability-studies/introduction/ "Introduction") 9 web essays from the volume, the list of [works cited](/disability-studies/works_cited/) for all the essays, information about the [contributors](/disability-studies/contributors/ "Contributors"), a [note on classroom use](/disability-studies/courses/in-the-classroom/note-on-classroom-use/ "Note on Classroom Use"). Any page in the site can be printed or saved as a pdf, and a single click provides a citation to that page that can be pasted into a bibliography.

### EXPLORE THE SITE

Readers may browse the full list of essays by clicking **Essays** at the upper left **to bring up a menu**. Clicking **Search** at upper right allows you to discover both the print and web essays: search results show the web essays in full and snippets from the print essays, with a page reference. This function enables readers to discover connections among the complete set of essays in this book.

In addition, the site enables readers to discover connections and contrasts across **all** the different Keywords volumes. Readers may select which books they want to explore. For example, if you select "all books" and search for "education," you'll see how that term is used not only in disability studies (**[education](/disability-studies/essay/education/)**), but also in Asian American studies (**[education](/asian-american-studies/essay/education/)**), children's literature (**[education](/childrens-literature/essay/education/)**), and environmental studies (**[education](/environmental-studies/essay/education/)**).

### COURSES

Instructors may wish to consult the [“Note on Classroom Use”](/disability-studies/courses/in-the-classroom/note-on-classroom-use/ "Note on Classroom Use") to find suggestions for how to employ this book in the classroom.

### THE BOOK

Readers can learn more about the print book, and purchase a copy of it from NYU Press, by clicking on the shopping cart logo at the top of each page.

### COMMUNICATION

You can share links using Facebook, Twitter, Google Plus, and e-mail from any page in this site.

