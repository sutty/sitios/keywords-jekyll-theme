---
title: About this site
layout: page
uuid: 2867f47f-23e0-47f9-a131-e9e1c4bc4203
last_modified_at: 2020-11-02 20:48:18.000000000 +00:00
book: f6b6d67a-7edb-4a7f-bb90-1d421f5af3ac
migration_id: 29
permalink: "/american-cultural-studies/about-this-site/"
---

_Keywords for American Cultural Studies, Third Edition_ is a hybrid print-digital publication that includes over 114 essays, each on a complex and contested term such as “disability,” “intersectionality,” “freedom,” and “prison.” The print volume includes 66 essays, 42 of which are new or revised for this second edition. Another 48 essays appear on this site.

The site also includes [“Keywords: An Introduction,"](/american-cultural-studies/keywords-an-introduction/) the list of [works cited](/american-cultural-studies/works_cited/) by all the essays, information about the [contributors](/american-cultural-studies/contributors/), a [note on classroom use](/american-cultural-studies/in-the-classroom/notes-on-classroom-use/), and [sample syllabi and assignments](/american-cultural-studies/in-the-classroom/syllabi-and-assignments/)<u>, a page that now includes a suggested assignment for an asynchronous online class</u>. Any page in the site can be printed or saved as a pdf, and a single click provides a citation to that page that can be pasted into a bibliography.

**Essays**

Readers may browse the full list of essays by clicking at the upper left. The search function allows you to shuttle between the print and web essays: search results show both the web essays in full and snippets from the print essays, with a page reference. This function enables readers to discover connections among the complete set of essays.

In addition to the essays published as part of this edition, we invite post-publication essays by students and scholars who want to respond to the essays by creating new work.

Finally the site enables readers to discover connections and contrasts across **all** the different Keywords volumes using the extraordinarily robust search function. For example, if you select "all books" and search for "gender," you'll see how that term is used not only in American studies and cultural studies ([**gender**](/american-cultural-studies/essay/gender/)), but also in related fields such as African American Studies, (**[gender](/african-american-studies/essay/gender/)**) Asian American studies, ([**gender**](/asian-american-studies/essay/gender/)) children's literature ([**gender**](/childrens-literature/essay/gender/)), comics studies (gender) disability studies ([**gender**](/disability-studies/essay/gender/)), Latina/o studies, and media studies. If you search a term that is not the title of an essay in a volume, you’ll also see how that term is used throughout the volumes -- including in the essays that appear only in print.

**Courses**

Thousands of students have read the first two editions of _Keywords for American Cultural Studies_ in courses across a wide range of disciplinary and interdisciplinary fields. This third print-digital edition is designed to increase this activity by collecting and learning from responses to the previous volumes. Instructors may wish to consult the [“Note on Classroom Use”](/american-cultural-studies/in-the-classroom/notes-on-classroom-use/) or examine [sample syllabi, suggested assignments](/american-cultural-studies/in-the-classroom/syllabi-and-assignments/), and other resources. Readers can learn more about the print book, and purchase a copy of it from NYU Press, by clicking on the shopping cart logo at the top of each page.

**Communication**

You can share links using Facebook, Twitter, and e-mail from any page on this site. Blog posts will update you on events related to the volumes in NYU Press’s _Keywords series._ Feel free to contact us at keywords@fordham.edu to ask questions about the projector to inquire about authoring a post-publication keyword essay.

**Acknowledgments**

This site is supported by NYU Libraries’ [Digital Library Technology Services (DLTS)](http://dlib.nyu.edu/dlts/), in collaboration with [NYU Press](http://nyupress.org). The project team was led Monica McCormick, with support from NYU Press by Adam Bohannan, Dorothea Halliday, Charles Hames, Alicia Nadkarni, Jodi Narde, Miguel Sandoval, and Eric Zinner; and from DLTS by Zach Coble, Laura Henze, Carol Kassel, David Millman, and Jennifer Vinopal. Crucial assistance was provided by intern Meghan White.

The site was designed by [Command C](http://www.commandc.com/).

