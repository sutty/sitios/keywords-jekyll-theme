---
title: Disability
layout: essay
uuid: 39e904a0-1a49-42a3-bd3f-964fa978cf58
last_modified_at: 2015-04-30 16:58:40.000000000 +00:00
book: 4b5534c6-21a6-4598-b134-d06081de34e6
migration_id: 11
availability: Web Essays
authors:
- 97a1cca3-58f4-405f-8b07-2f40c98418d1
permalink: "/asian-american-studies/essay/disability/"
works_cited:
- d5db200a-1a78-4b36-8d23-7743200aec58
- 26b592fd-3cad-4ab8-ba3b-5777f9fee798
- c4210cb3-61cf-403d-afed-6127fae8b59d
- 36ef4058-3780-4a54-89b0-bde546d5549f
- 0b69d573-449c-46f4-96da-d50d68d68211
- c4c1660c-0e3e-44d3-8d99-6a860f1ab776
- c69d7a5c-f996-4d84-b674-de29d320ba55
- 376563df-ee3b-4c71-a79d-05c28bf6460f
- 28ba6f77-02a9-4a12-83d8-f3a659d2f6f1
- 8f3505cd-8602-4d23-b057-27832e91d7c1
- 575a5adf-97fd-4744-a3b0-35021a2f78ce
- 352baf54-d1ca-49f7-be3a-297dc1b335a3
- e0a60138-698f-43cf-8334-e1eaf15fc9f1
- 43ff37ce-1529-4d8c-9243-698072ef5ee7
- 4250ef64-7d61-4a7e-9691-7bd509b4249b
- 4387826e-0b09-43f4-96f2-451720a3952c
- 4d328565-d9a1-4bd6-b57e-d88484c7409e
- 533a4608-d66f-4e55-a755-532301fc02fd
- fa24d739-2891-45aa-be23-db9d6331c144
- 8201fd95-8c12-47ee-a9a6-1e7b0575b7e6
- 4888cf63-6031-4db3-a4a8-e9eeb30e23e3
- f407855a-2c97-41a5-8904-0abdfbd85599
---

According to the _Oxford English Dictionary_, the first appearance of “disability” occurred in the mid-­sixteenth century. Its adjectival form, “disabled,” follows shortly thereafter in the linguistic record. It appears that from the beginning, the three definitions of disability that persist today—­“a lack of ability (to discharge any office or function),” “a physical or mental condition that limits a person’s movements, senses, or activities,” and “a restriction framed to prevent any person or class of persons from sharing in duties and privileges which would otherwise be open to them”—­coexisted with one another. A now-­obsolete meaning, disability as financial hardship, disappeared from use in the nineteenth century.

The field of Asian American studies has seen a recent surge of scholarship that addresses disability. A Modern Language Association convention panel, a special issue of _Amerasia Journal_, and several monographs—­all appearing in the past few years—­together mark this acceleration of interest. Although ethnic studies was, at first, somewhat slow to initiate dialogue with disability studies, the conversations that scholars have generated of late speaks to the shared intellectual and political commitments of these fields.

Disability studies was founded in the 1990s in ways that reflected the cultural changes in the wake of the Americans with Disabilities Act of 1990. The legislation—­which prohibited discrimination based on ability status and mandated reasonable accommodations in education, employment, public facilities, and commercial services for disabled people—­actualized a long effort on the part of activists that began during the civil rights era. Disability studies, as a discipline in the humanities, differentiates itself from the fields of rehabilitation medicine (such as physical therapy or occupational therapy) by locating its critique within the social and built environments that create incapacitating barriers for disabled people. Instead of developing therapies to normalize people, the field focuses on the social justice implications of unequal access. Consequently, it adopts a methodology that privileges the cultural meanings of physical, sensory, and neural difference rather than treatment and cure.

Despite the presence of disability—­as evidenced in the linguistic record—­as a social and cultural entity from the mid-­1500s onward, it was not until the advent of modernity that differences marked by ability status were regarded and handled in the manner that is familiar to us now. The standardizing discourses and practices associated with empiricism, urbanization, and industrialization occasioned a shift from a society where human variation was integrated into everyday life to one where forms of intervention and control—­linguistic, educational, spatial, medical, and legal—­were leveled upon disabled people ([Bogdan 1990;](/asian-american-studies/works_cited/bogdan-robert/ "Bogdan, Robert.") [Davis 1995;](/asian-american-studies/works_cited/davis-lennard-j/ "Davis, Lennard J.") [Trent 1995;](/asian-american-studies/works_cited/trent-james/ "Trent, James.") [Baynton 1996;](/asian-american-studies/works_cited/baynton-douglas/ "Baynton, Douglas") [Reiss 2008;](/asian-american-studies/works_cited/reiss-benjamin/ "Reiss, Benjamin.") [Schweik 2010;](/asian-american-studies/works_cited/schweik-susan/ "Schweik, Susan.") [Rembis 2011;](/asian-american-studies/works_cited/rembis-michael/ "Rembis, Michael.") [Nielsen 2012](/asian-american-studies/works_cited/neilsen-kim/ "Neilsen, Kim.")). The historical effects of this segregation, forced or coerced therapy, juridical abuse, and cultural erasure are what activists and scholars are still attempting to expose and redress today.

This framework—­which posits disability as difference that demands accommodation in the form of institutional change rather than assimilation and integration—­would be familiar to scholars of race. The emergence of ethnic studies as an academic discipline in the late 1960s and early 1970s took place in tandem with concurrent social movements outside of the academy that challenged racism, class inequality, and militaristic imperialism. The various racial liberatory movements and the opposition to the Vietnam War marked a departure from the Cold War conformity of the previous generation.

The rise of a sustained panethnic and cross-­racial Asian American movement during this era privileged a heteropatriarchal—­and, by extension, a nondisabled—­subject. The early activists attempted to generate their critiques of social inequality by appealing to standards of normative masculinity. The disavowal of gender and sexual difference in cultural nationalist politics has been well documented in Asian American studies. However, the lack of a corresponding body of work that unpacks ableism in Asian American cultural nationalism is striking, given how closely the discourses of gender, sexuality, and disability are intertwined.

Correspondingly, disability studies has faced critiques of its white normativity ([Bell 2006](/asian-american-studies/works_cited/bell-chris/ "Bell, Chris.")). Its areas of inquiry and the demographic composition of its practitioners have assumed a whiteness that marginalized scholars who maintained intellectual commitments to race. Nevertheless, there are a few seminal texts where we can see the earliest examples of disability and race/ethnicity overlapping and/or mutually constituting each another ([Gilman 1985;](/asian-american-studies/works_cited/gilman-sander/ "Gilman, Sander.") [Gilman 1996;](/asian-american-studies/works_cited/gilman-sander-se/ "Gilman, Sander.SE") [Kraut 1995;](/asian-american-studies/works_cited/kraut-alan-m/ "Kraut, Alan M.") [Garland-­Thomson 1997](/asian-american-studies/works_cited/garland-thomson-rosemarie/ "Garland-Thomson, Rosemarie.")). This methodology extends itself more explicitly in the work that follows ([James and Wu 2006;](/asian-american-studies/works_cited/james-jennifer-and-cynthia-wu-eds/ "James, Jenniferand Cynthia Wu, eds.") [James 2007;](/asian-american-studies/works_cited/james-jennifer/ "James, Jennifer.") [C. Wu 2012;](/asian-american-studies/works_cited/wu-cynthia/ "Wu, Cynthia.") [Ho and Lee 2013;](/asian-american-studies/works_cited/ho-jennifer-and-james-kyung-jin-lee-eds/ "Ho, Jennifer, and James Kyung-Jin Lee, eds.") [Minich 2013](/asian-american-studies/works_cited/minich-julie/ "Minich, Julie.")). The challenge for scholars as this line of inquiry moves forward, especially in the field of Asian American studies, is to explore how these interpretive lenses can be repurposed to go beyond—­but not transcend—­a predictable archive. Such an approach might follow the dictum that we acknowledge but not hierarchize the tension between seeing matters of political difference as particular to social minorities and seeing difference as integrated into the universality of human experience ([Sedgwick 1990](/asian-american-studies/works_cited/sedgwick-eve-kosofsky/ "Sedgwick, Eve Kosofsky.")) or the proposition that we evacuate the subject of analysis altogether and define our field by mode of critique ([Chuh 2003](/asian-american-studies/works_cited/chuh-kandice/ "Chuh, Kandice.")).

Examples of recent work that performs these analytical maneuvers include a literary critical examination of fiction responding to the neocolonial ties between the United States and India that were exposed after the 1984 Union Carbide gas leak in Bhopal ([Jina Kim 2014](/asian-american-studies/works_cited/kim-jina/ "Kim, Jina.")). The workings of multinational corporations, which cheapen some lives in return for the comfort of others, force a reconsideration of the logic of disability activism and disability studies in the global North. Also notable is a study of how contemporary concepts of toxicity are transposed onto historically sedimented anxieties about a transnational Asia ([M. Chen 2012](/asian-american-studies/works_cited/chen-mel/ "Chen, Mel.")). Fears about racialized contaminants arise out of the ambivalence that North Americans hold about the movement of bodies, objects, and capital alike across national borders. These are some of the possibilities that future work on disability may conjure in the field of Asian American studies.

