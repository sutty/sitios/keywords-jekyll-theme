---
title: Note on Classroom Use
layout: page
uuid: 8c5dd987-2cf4-4963-8547-c6281c9f95c1
last_modified_at: 2015-04-07 17:25:55.000000000 +00:00
book: 4b5534c6-21a6-4598-b134-d06081de34e6
migration_id: 144
authors:
- 6266a1ba-bb27-4ea6-940d-d67286973301
- bcff61cf-49bb-4693-9233-3aa4b4ef3b11
- d1f060bc-2aea-4928-83fa-4a441348e3d3
permalink: "/asian-american-studies/in-the-classroom/note-on-classroom-use/"
---

As we assert in the introduction to _Keywords for Asian American Studies_, this volume – like other volumes in the series – is not an encyclopedia. Rather, _Keywords for Asian American Studies_ is inspired and shaped by Raymond Williams’s still-relevant contention that various terms represent a flexible yet identifiable vocabulary that “has been inherited within precise historical and social conditions” that nevertheless must “be made at once conscious and critical” (1985, 24). _Keywords for Asian American Studies_ likewise reflects the contours of a multidisciplinary field that encompasses the social sciences, humanities, and cultural studies.

While we encouraged contributors to think capaciously with regard to a term’s currency across multiple disciplines, we urge you to think thematically with regard to the collection. Correspondingly, _Keywords for Asian American Studies_ highlights different approaches, methodologies, and frames to consider what has become a historical set of field standards: various exclusionary acts (for instance, the 1882 Chinese Exclusion Act and Supreme Court rulings on citizenship (non)eligibility), state-authorized relocation and imprisonment (such as the World War II-era Japanese American incarceration/internment), U.S. foreign policy (in countless wars “over there”), domestic racial conflict (like the 1992 LA Riots/Uprising), and immigration shifts (namely the 1965 Immigration and Nationality Act and the 1980 Refugee Act). Such organizational frames and recurring themes cohere with what we note in our introduction as a central aim: “delineate the contours of Asian America as an imagined and experienced site.” Such a goal initially drew us to what Sucheng Chan productively noted in _Asian Americans: An Interpretive History_ (1991) as an interpretive space marked by “prejudice, economic discrimination, political disenfranchisement, physical violence, immigration exclusion, social segregation, and incarceration” (45). Using these markers or themes to construct a course allows for a pedagogical usage that highlights the interconnections between groupings and links the lives of Asian American across temporal and spatial locations.

_Keywords_ can be primary text or a supplementary reading in an undergraduate course or graduate seminar in which the instructors and the students can read the keyword essays in any order or combination. Exercises can evolve around pairing keywords or linking multiple keywords, allowing readers to converse on how the concepts are aligned or associated with one another, and perhaps, how the essays might reflect varying interpretations or directions of the field. The essays can be utilized to frame a classroom conversation throughout the quarter/semester, strategically organize a research project, or complement other specific course readings, even a book by one of the essay contributors. Students may be motivated to conduct further research using the references within an essay and write an alternative keyword essay or even be inspired to identify and write their own keyword essay. We therefore intend this collection to be one that is translated and interpreted in ways that both fit and expand the boundaries of the social sciences classroom, the humanities lesson plan, and the cultural studies unit. We hope that _Keywords_ lends itself to enhancing experimentations in the classroom and look forward to learning about your pedagogical adventures.

