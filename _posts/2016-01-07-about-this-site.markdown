---
title: About this Site
layout: page
uuid: ac18dbc9-44d1-4f66-b6d2-922368650457
last_modified_at: 2016-02-02 20:01:55.000000000 +00:00
book: b0b5db72-8351-4602-a031-ac9c06e636cc
migration_id: 8
authors:
- 67e9f480-47be-4364-bf1d-e5046461a622
- 18e21834-b5b7-40ef-8431-76ea0cca1671
- 0e327fcb-6cf6-43ac-9739-2e0fc628dc8b
permalink: "/environmental-studies/about-this-site/"
---

_Keywords for Environmental Studies_ analyzes the central terms and debates currently structuring the most exciting research in and across environmental studies, including the environmental humanities, environmental social sciences, sustainability sciences, and the sciences of nature.

The print publication includes sixty essays from humanists, social scientists, and scientists, each written about a single term, reveal the broad range of quantitative and qualitative approaches critical to the state of the field today. From “**[ecotourism](/environmental-studies/essay/ecotourism/)”** to **“[ecoterrorism](/environmental-studies/essay/ecoterrorism/),”** from **“[genome](/environmental-studies/essay/genome/)”** to **“[species](/environmental-studies/essay/species/),”** this accessible volume illustrates the ways in which scholars are collaborating across disciplinary boundaries to reach shared understandings of key issues—such as extreme weather events or increasing global environmental inequities—in order to facilitate the pursuit of broad collective goals and actions. This site includes the volume’s **“[Introduction](/environmental-studies/introduction-2/),”** 7 web essays from the volume, the list of **[works cited](/environmental-studies/works-cited/)** for all the essays, information about the **[contributors](/environmental-studies/contributors/)**, a **[note on classroom](/environmental-studies/in-the-classroom/note-on-classroom-use/)** use. Any page in the site can be printed or saved as a PDF, and a single click provides a citation to that page that can be pasted into a bibliography.

### EXPLORE THE SITE

Readers may browse the full list of essays by clicking **Essays** at the upper left **to bring up a menu**. Clicking **Search** at upper right allows you to discover both the print and web essays: search results show the web essays in full and snippets from the print essays, with a page reference. This function enables readers to discover connections among the complete set of essays in this book.

In addition, the site enables readers to discover connections and contrasts across **all** the different Keywords volumes. Readers may select which books they want to explore. For example, if you select "all books" and search for "education," you'll see how that term is used not only in environmental studies (**[education](/environmental-studies/essay/education/)**) but also in Asian American studies (**[education](/asian-american-studies/essay/education/)**), children's literature (**[education](/childrens-literature/essay/education/)**), and disability studies (**[education](/disability-studies/essay/education/)**).

### COURSES

Instructors may wish to consult the **“[Note on Classroom Use](/environmental-studies/in-the-classroom/note-on-classroom-use/)”** to find suggestions for how to employ this book in the classroom.

### THE BOOK

Readers can learn more about the print book, and purchase a copy of it from NYU Press, by clicking on the shopping cart logo at the top of each page.

### COMMUNICATION

You can share links using Facebook, Twitter, Google Plus, and e-mail from any page in this site.
